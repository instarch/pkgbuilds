#!/bin/bash

source /etc/conf.d/flynn-host

_GW=$(ip route | awk '/^default/ {print $3}')
IP_ADDRESS=$(ip route get "${_GW}" | awk '/src/ {print $NF}')

/usr/bin/flynn-host daemon \
    --external ${IP_ADDRESS} \
    ${FLYNN_HOST_ARGS}
